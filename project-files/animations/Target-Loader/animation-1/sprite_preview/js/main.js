var config = {
  type: Phaser.AUTO,
  parent: 'phaser-example',
  width: window.innerWidth,
  height: window.innerHeight,
  autoResize: true,
  scene: {
    preload: preload,
    create: create,
    //update: update,
    resize: resize
  }
};

function preload (){
  this.load.path = 'assets';
  //Load images
  var images=['bg.png'];
  for(var i=0;i<images.length;i++){
    var name = images[i].split(".")[0];
    this.load.image(name, '/images/'+images[i]);
  }

  //Load Sprites
  var sprites=['sprite_1.png'];
  for(var i=0;i<sprites.length;i++){
    var name = sprites[i].split(".")[0];
    this.load.image(name, '/sprites/'+sprites[i]);
  }

}

function create (){
  bg = this.add.image(0, 0, 'bg').setOrigin(0).setDisplaySize(game.config.width, game.config.height);
  sprite_1 = this.add.sprite(game.config.width / 2, game.config.height / 2, 'sprite_1');
  sprite_1.setScale(0.5);

  this.events.on('resize', resize, this);
}

function resize (width, height){
  if (width === undefined) { width = game.config.width; }
  if (height === undefined) { height = game.config.height; }

  this.cameras.resize(width, height);

  bg.setDisplaySize(width, height);
  sprite_1.setPosition(width / 2, height / 2);
}

var game = new Phaser.Game(config);

window.addEventListener('resize', function (event) {

  game.resize(window.innerWidth, window.innerHeight);

}, false);
